import os
from krita import *


class LayerNotFoundError(Exception):
    pass

class ExporterExtension(Extension):
    def __init__(self, parent):
        super().__init__(parent)

    def export(self):
        Application.setBatchmode(True) # don't show any popups
        application = Krita.instance()

        print("Hello :3")

        def exportLayer(doc,layer, use_layer_name = True):    
            name = os.path.basename(doc.fileName()).replace(".kra","")
            directory = os.path.dirname(doc.fileName())
            export_dir = os.path.join(directory, "exported")

            if not os.path.isdir(export_dir):
                os.makedirs(export_dir)
            
            path = ""
            if use_layer_name:
                path = os.path.join(export_dir,f"{name} {layer.name()}.png")
            else: 
                path = os.path.join(export_dir,f"{name}.png")
                
            prev_opacity = layer.opacity()
            layer.setOpacity(255)
            layer.save(path, doc.resolution(), doc.resolution(), InfoObject())
            layer.setOpacity(prev_opacity)

        def exportFromDocument(doc):
            missing_layers = []
            for expected_name in ["kontur", "kolor", "outline"]:
                if doc.nodeByName(expected_name) is None:
                    missing_layers.append(expected_name)
            if len(missing_layers) > 0:
                layerList = ", ".join([f"'{x}'" for x in missing_layers])
                s = "s" if len(missing_layers) > 0 else ""
                raise LayerNotFoundError(f"Layer{s}: {layerList} were not found in '{os.path.basename(doc.fileName())}'!")
            


            kontur_layer = doc.nodeByName("kontur")
            kontur_kolorowy_layer = doc.nodeByName("kontur kolorowy")
            kolor_layer = doc.nodeByName("kolor")
            outline_layer = doc.nodeByName("outline")


            selected_layer = kontur_layer
            if kontur_kolorowy_layer is not None:
                selected_layer = kontur_kolorowy_layer 

            kolor_duplicate = kolor_layer.duplicate()
            selected_duplicate = selected_layer.duplicate()

            root = doc.rootNode()
            root.addChildNode(kolor_duplicate, kolor_layer)
            root.addChildNode(selected_duplicate, kolor_duplicate)

            kolor_duplicate.setName("combined")
            selected_duplicate.mergeDown()

            exportLayer(doc,doc.nodeByName("outline"))
            exportLayer(doc,doc.nodeByName("kontur"))
            exportLayer(doc,doc.nodeByName("combined"), False)
            doc.nodeByName("combined").remove()
            doc.save()    

        skip = False
        aborted = False
        success_count = 0
        for document in application.documents():
            try:
                exportFromDocument(document)
                success_count += 1
            except LayerNotFoundError as err:
                if skip:
                    continue
                err_box = QMessageBox()
                err_box.setWindowTitle("Error")
                err_box.setText(f"{err}\nDo you want to continue?")
                err_box.setIcon(QMessageBox.Critical)
                err_box.setStandardButtons(QMessageBox.No | QMessageBox.Yes | QMessageBox.YesToAll)
                err_box.exec_()
                if err_box.clickedButton() is err_box.button(QMessageBox.No):
                    aborted = True
                    break
                elif err_box.clickedButton() is err_box.button(QMessageBox.Yes):
                    continue
                elif err_box.clickedButton() is err_box.button(QMessageBox.YesToAll):
                    skip = True
                    continue
        end_box = QMessageBox()
        message = "Aborted!" if aborted else "All done :3"
        end_box.setText(f"{message}, exported {success_count}/{len(application.documents())}")
        end_box.setWindowTitle("Finished")
        end_box.setIcon(QMessageBox.Information)
        end_box.setStandardButtons(QMessageBox.Ok)
        end_box.exec_()

        Application.setBatchmode(False)

    def setup(self):
        pass

    def createActions(self, window):
        export_action = window.createAction(
            "export_for_work",
            "Export (for work)",
            "tools/scripts"
        )
        export_action.triggered.connect(self.export)

Krita.instance().addExtension(ExporterExtension(Krita.instance()))
